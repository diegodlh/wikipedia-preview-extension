# Wikipedia Preview Extension

[Wikipedia Preview](https://www.mediawiki.org/wiki/Wikipedia_Preview) is a script that can be loaded into webpages so that a preview of Wikipedia articles is shown when the user hovers over Wikipedia links. It has been developed by the Wikimedia's Foundation Inuka team.

However, the original project relies on the webpage owners to include the script on their webpages.

This repository is meant to provide ways for users to embed the script on the webpages they want to.

![Wikipedia Preview popup](wikipedia-preview.png)

## Bookmarklet
One option is via a [bookmarklet](https://en.wikipedia.org/wiki/Bookmarklet). That is, a browser bookmark that instead of pointing to a URL runs a JavaScript code on the current page.

![Wikipedia Preview bookmarklet](bookmarklet.png)

To install this bookmarklet:

1. Create a new bookmark in your browser and give it a name. For example "Wikipedia Preview". You may consider adding it to your bookmark bar for easier access.
2. In the bookmark's target URL field enter the following code (you may modify the `config` object according to the Wikipedia Preview's [documentation](https://github.com/wikimedia/wikipedia-preview) for customization):

```
javascript:(function(){
    let config = {
        root: document,
        detectLinks: true
    };
    let scriptElem = document.createElement("script");
    scriptElem.setAttribute("src", "https://unpkg.com/wikipedia-preview/dist/wikipedia-preview.production.js");
    document.body.appendChild(scriptElem);
    scriptElem.onload = () => {
        wikipediaPreview.init(config);    
    }
})();
```

Note this won't work on pages with strict [Content Security Policy `script-src`](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/script-src) settings.

## User Script
If you want Wikipedia Preview automatically injected on all webpages you visit, you may install [this](https://greasyfork.org/en/scripts/466770-wikipedia-preview-user-script) user script using a user script manager such as [Tampermonkey](https://www.tampermonkey.net/).

Note that this is currently not working with Greasemonkey, probably because of https://github.com/greasemonkey/greasemonkey/issues/2549.

## Browser Extension
Moving forward, a browser extension could simply be created that automatically injects Wikipedia Preview on all webpages. This would make installation easier and more widely available, and would support configuring the tool from the extension's preferences.

However, note this would usually not be available for mobile browsers, since they don't usually support extensions, or do so limitedly.

## License
This work is released under the terms of GPL-3.0 or any later version.
